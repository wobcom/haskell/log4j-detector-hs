{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Codec.Archive.Zip
import           Control.Applicative (liftA2)
import           Control.Monad.IO.Class (MonadIO, liftIO)
import qualified Data.ByteString as BS
import           Data.Char (toLower)
import           Data.Foldable (foldrM, traverse_)
import           Data.List (isSuffixOf)
import qualified Data.Map as M
import           System.Directory (canonicalizePath, getTemporaryDirectory,
                                   listDirectory, removeFile)
import           System.Environment (getArgs)
import           System.IO (Handle, hClose, hPutStrLn, openBinaryTempFile,
                            stderr, stdout)

import           Control.Exception (SomeException(..), bracket, catch,
                                    displayException, handle)
import           System.Posix.Files

main :: IO ()
main = do
  paths <- getArgs
  traverse_ (traverseFiles scan) paths

scan :: FilePath -> IO ()
scan path | isZip path = scanJar path path
          | otherwise = pure ()

data ScannerState = ScannerState
  { ssLog4j_v2_markers :: Int
  , ssLog4j_v1_markers :: Int
  , ssLog4j_min_v2_0b9_marker :: Bool
  , ssLog4j_min_v2_10_marker :: Bool
  , ssLog4j_min_v2_15_marker :: Bool
  , ssLog4j_mitigate :: Todo
  } deriving (Eq, Show)


data Report = Report Version Todo deriving (Eq, Show)

data Todo = Mitigate | MaybeMitigate | Done deriving (Eq, Show)

data Version
  = V1
  | V2_lte_2_0b8
  | V2_gte_2_0b9_lt_2_10
  | V2_gte_2_10_lt_2_15
  | V2_gte_2_15
  deriving (Eq, Show)

is_v1 :: ScannerState -> Bool
is_v1 s = ssLog4j_v1_markers s == 4

is_v2 :: ScannerState -> Bool
is_v2 s = ssLog4j_v2_markers s == 5

-- | Return Nothing if this does not appear to contain log4j
scanResult :: ScannerState -> Maybe (Either String Report)
scanResult s | s == initialState
               = Nothing

               | not (is_v1 s)
               , not (is_v2 s)
               = Nothing

               | is_v1 s
               , is_v2 s
               = Just $ Left ("Conflicting marker set: " <> show s)

               | is_v1 s
               = Just $ Right (Report V1 mit)

               | ssLog4j_min_v2_0b9_marker s
               , ssLog4j_min_v2_10_marker s
               , ssLog4j_min_v2_15_marker s
               = Just $ Right (Report V2_gte_2_15 mit)

               | ssLog4j_min_v2_0b9_marker s
               , ssLog4j_min_v2_10_marker s
               = Just $ Right (Report V2_gte_2_10_lt_2_15 mit)

               | ssLog4j_min_v2_0b9_marker s
               = Just $ Right (Report V2_gte_2_0b9_lt_2_10 mit)

               | not (ssLog4j_min_v2_0b9_marker s)
               = Just $ Right (Report V2_lte_2_0b8 mit)

               | otherwise
               = Just $ Left ("Conflicting marker set: " <> show s)
  where
    mit = ssLog4j_mitigate s

describeReport :: Report -> String
describeReport (Report v mit) = vers v <+> prettyMit
  where
    vers V1                   = "version 1, potentially vulnerable."
    vers V2_lte_2_0b8         = "version 2, <= 2.0-beta8, potentially vulnerable."
    vers V2_gte_2_0b9_lt_2_10 = "version 2, >= 2.0-beta-9, < 2.10.0, vulnerable."
    vers V2_gte_2_10_lt_2_15  = "version 2, >= 2.10, < 2.15.0, vulnerable."
    vers V2_gte_2_15          = "version 2, >= 2.15.0."


    prettyMit = case mit of
      Mitigate      -> "MITIGATION NEEDED!"
      MaybeMitigate -> "mitigation potentially needed"
      Done          -> "appears mitigated, all done."

initialState :: ScannerState
initialState = ScannerState
    { ssLog4j_v2_markers = 0
    , ssLog4j_v1_markers = 0
    , ssLog4j_min_v2_0b9_marker = False
    , ssLog4j_min_v2_10_marker = False
    , ssLog4j_min_v2_15_marker = False
    , ssLog4j_mitigate = MaybeMitigate
    }


(<+>) :: String -> String -> String
l <+> r = l <> " " <> r

scanJar :: String -> FilePath -> IO ()
scanJar human path = handle onError (withArchive path scan)
  where
    onError :: SomeException -> IO ()
    onError ex = warn ("Failed to scan archive " <> human <+> displayException ex)

    scan :: ZipArchive ()
    scan = do
       assoc <- M.toList <$> getEntries
       s <- foldrM handleEntry initialState assoc
       case scanResult s of
         Nothing -> pure () -- No log4j indicators
         Just (Left err) -> bark ("Log4j markers found, but errors found: " <> err)
         Just (Right ver) -> bark (describeReport ver)


    bark :: MonadIO m => String -> m ()
    bark what = note (human <> " " <> what)

    handleEntry :: (EntrySelector, EntryDescription) -> ScannerState -> ZipArchive ScannerState
    handleEntry (selector, _descr) state = do
        let path = unEntrySelector selector
        case () of
          _ | isZip path
            -> state <$ (liftIO . followAlice path =<< getEntry selector)

          _ | otherwise
            -> scanMarker state selector

    ignoringIOErrors :: IO () -> IO ()
    ignoringIOErrors act = act `catch` (\e -> const (return ()) (e :: IOError))

    -- Given a binary buffer that looks like a zip, try and recurse.
    followAlice :: FilePath -> BS.ByteString -> IO ()
    followAlice name buf = bracket
        (newBinaryTempFile "log4jscanner")
        (\(name, handle) -> hClose handle >> ignoringIOErrors (removeFile name))
        (\(name, handle) -> go name handle )
      where
        go :: FilePath -> Handle -> IO ()
        go tmpPath handle = do
            BS.hPut handle buf
            hClose handle
            scanJar (human <> "!" <> name) tmpPath

    newBinaryTempFile :: FilePath -> IO (FilePath, Handle)
    newBinaryTempFile path = getCanonicalTemporaryDirectory >>= \tmp -> openBinaryTempFile tmp path

    getCanonicalTemporaryDirectory :: IO FilePath
    getCanonicalTemporaryDirectory = getTemporaryDirectory >>= canonicalizePath

    scanMarker :: ScannerState -> EntrySelector -> ZipArchive ScannerState
    scanMarker s sel =
        case toLower <$> unEntrySelector sel of
               -- v2 marker
          name | "core/logevent.class" `isSuffixOf` name
               -> pure $ s { ssLog4j_v2_markers = ssLog4j_v2_markers s + 1 }

               | "core/appender.class" `isSuffixOf` name
               -> pure $ s { ssLog4j_v2_markers = ssLog4j_v2_markers s + 1 }

               | "core/filter.class" `isSuffixOf` name
               -> pure $ s { ssLog4j_v2_markers = ssLog4j_v2_markers s + 1 }

               | "core/layout.class" `isSuffixOf` name
               -> pure $ s { ssLog4j_v2_markers = ssLog4j_v2_markers s + 1 }

               | "core/loggercontext.class" `isSuffixOf` name
               -> pure $ s { ssLog4j_v2_markers = ssLog4j_v2_markers s + 1 }

               -- v1 marker
               | "net/jmsappender.class" `isSuffixOf` name
               -> pure $ s { ssLog4j_v1_markers = ssLog4j_v1_markers s + 1 }

               | "net/syslogappender.class" `isSuffixOf` name
               -> pure $ s { ssLog4j_v1_markers = ssLog4j_v1_markers s + 1 }

               | "fileappender.class" `isSuffixOf` name
               -> pure $ s { ssLog4j_v1_markers = ssLog4j_v1_markers s + 1 }

               | "logmanager.class" `isSuffixOf` name
               -> pure $ s { ssLog4j_v1_markers = ssLog4j_v1_markers s + 1 }


               | "appender/nosql/nosqlappender.class" `isSuffixOf` name
               -> pure $ s { ssLog4j_min_v2_10_marker = True }

               | "jndilookup.class" `isSuffixOf` name
               -> do buf <- getEntry sel
                     -- log4j2 commit f819c8380 removed this particular line. This is the first commit in 2.12.2 which is a backport fix for 2.12
                     let r = "Error looking up JNDI resource [{}]." `BS.isInfixOf` buf
                     pure $ s { ssLog4j_min_v2_0b9_marker = True
                              , ssLog4j_mitigate = if r then Mitigate else Done
                              }

               | "jndimanager.class" `isSuffixOf` name
               -> do buf <- getEntry sel
                     -- log4j2 commit c2b07e3799 introduced a warning that emits this string. That means we can find this string in the compiled java class.
                     -- The first release that comes with this commit is 2.15 and thus has the necessary mitigations
                     let r = "Invalid JNDI URI - {}" `BS.isInfixOf` buf
                     pure $ s { ssLog4j_min_v2_15_marker = r
                              , ssLog4j_mitigate = if r then Done else Mitigate
                              }

               | otherwise
               -> pure s

note :: MonadIO m => String -> m ()
note = liftIO . hPutStrLn stdout

warn :: MonadIO m => String -> m ()
warn = liftIO . hPutStrLn stderr

(</>) :: FilePath -> FilePath -> FilePath
l </> r = l <> "/" <> r

traverseFiles :: (FilePath -> IO ()) -> FilePath -> IO ()
traverseFiles g path = handle onError $ do
    fs <- getSymbolicLinkStatus path

    case () of
        _ | isRegularFile fs -> g path
        _ | isDirectory fs   -> do
            ps <- listDirectory path
            traverse_ (\sub -> traverseFiles g (path </> sub)) ps
        _ | otherwise        -> pure ()

  where
    onError :: SomeException -> IO ()
    onError ex = warn ("Failed to access " <> path <> ": " <> displayException ex)

(.||.) :: (a -> Bool) -> (a -> Bool) -> a -> Bool
(.||.) = liftA2 (||)

isZip :: FilePath -> Bool
isZip f = pred (toLower <$> f)
  where
    pred = isSuffixOf ".zip"
      .||. isSuffixOf ".war"
      .||. isSuffixOf ".ear"
      .||. isSuffixOf ".jar"
      .||. isSuffixOf ".aar"
