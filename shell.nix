{ pkgs ? import <nixpkgs> {}
}:

let libraries = [
    pkgs.zlib
    pkgs.bzip2
    pkgs.postgresql
    pkgs.pcre
  ];
in
pkgs.stdenv.mkDerivation rec {
  name = "log4j-detector-hs";

  buildInputs = [
    pkgs.haskell.compiler.ghc8104
    pkgs.haskell.packages.ghc8104.haskell-language-server
    pkgs.cabal-install
    pkgs.haskellPackages.cabal-plan
    pkgs.python39
  ] ++ libraries;

  shellHook = ''
    export LD_LIBRARY_PATH=${pkgs.lib.makeLibraryPath libraries}:$LD_LIBRARY_PATH
    export LANG=en_US.UTF-8
  '';
}
